package ma.octo.assignement.web;

import ma.octo.assignement.dto.VersementDTO;
import ma.octo.assignement.entities.Compte;
import ma.octo.assignement.entities.Versement;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.service.AutiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/versements")
public class VersementController {

    Logger LOGGER = LoggerFactory.getLogger(VersementController.class);

    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private VersementRepository versementRepository;
    @Autowired
    private AutiService autiService;
    @Autowired
    private UtilisateurRepository utilisateurRepository;


    @PostMapping("/executerVersement")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransactionVirement(@RequestBody VersementDTO versementdto) throws CompteNonExistantException, TransactionException {
        Compte compte = compteRepository.findByRib(versementdto.getRib());
        compte.setSolde(compte.getSolde().add(versementdto.getMontantVirement()));
        compteRepository.save(compte);
        Versement versement = new Versement();
        versement.setDateExecution(versementdto.getDateExecution());
        versement.setCompteBeneficiaire(compte);
        versement.setMontantVirement(versementdto.getMontantVirement());
        versement.setMotifVersement(versementdto.getMotifVersement());
        versementRepository.save(versement);
        autiService.auditVersement("Versement par " + versementdto.getNom_prenom_emetteur() + " vers " + versementdto.getRib()
                + " d'un montant de " + versement.getMontantVirement()
                .toString());
    }

}
