package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.util.Date;

public class VersementDTO {


    private BigDecimal montantVirement;

    private Date dateExecution;

    private String nom_prenom_emetteur;

    private String rib;

    private String motifVersement;


    public BigDecimal getMontantVirement() {
        return montantVirement;
    }

    public void setMontantVirement(BigDecimal montantVirement) {
        this.montantVirement = montantVirement;
    }

    public Date getDateExecution() {
        return dateExecution;
    }

    public void setDateExecution(Date dateExecution) {
        this.dateExecution = dateExecution;
    }

    public String getNom_prenom_emetteur() {
        return nom_prenom_emetteur;
    }

    public void setNom_prenom_emetteur(String nom_prenom_emetteur) {
        this.nom_prenom_emetteur = nom_prenom_emetteur;
    }

    public String getRib() {
        return rib;
    }

    public void setRib(String rib) {
        this.rib = rib;
    }

    public String getMotifVersement() {
        return motifVersement;
    }

    public void setMotifVersement(String motifVersement) {
        this.motifVersement = motifVersement;
    }
}
